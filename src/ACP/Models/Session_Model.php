<?php

namespace ACP\Models;

class Session_Model extends Core_Model
{
	var $table = 'sessions';

	var $auto_increment = FALSE;
	var $use_guid = FALSE;

	/* ALTER TABLE  `dutchlady_contest_dev`.`1504_dutch_lady_player_phase_two` ADD INDEX (  `fb_id` ) */
	var $pk_field = 'session_id';
	
	var $mapping_field  = array('session_id');

	var $table_indexes = array(
		array('user_id'),
	);

	var $fields_details = array(
		'session_id' => array(
			'type'           => 'VARCHAR',
			'constraint'     => 40,
			'pk'			 => TRUE,
		),

		'user_id' => array(
			'type'           => 'VARCHAR',
			'constraint'     => 40,
		),

		'ip_address'=> array(
			'type'           => 'VARCHAR',
			'constraint'     => 50,
			'control'		 => 'plain',
			'listing'		 => TRUE,
		),

		'user_agent'=> array(
			'type'           => 'VARCHAR',
			'constraint'     => 255,
			'control'		 => 'plain',
			'listing'		 => TRUE,
		),

		'last_activity'=> array(
			'type'           => 'BIGINT',
			'constraint'     => 20,
			'control'		 => 'text',
			'listing'		 => TRUE,
		),

		'user_data'=> array(
			'type'           => 'TEXT',
			'control'		 => 'textarea',
			'listing'		 => FALSE,
		),

	);

	protected function selecting_options($options = false) {
		parent::selecting_options($options);


	}
}